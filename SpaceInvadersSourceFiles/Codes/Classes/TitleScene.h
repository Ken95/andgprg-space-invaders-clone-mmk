#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

class TitleScene : public Layer
{
public:
	static Scene* createScene();

	virtual bool init();

	CREATE_FUNC(TitleScene);

private:
	Size mVisibleSize;
	Vec2 mOrigin;

	void mTransitionToPlay(Ref* sender);
	void mTransitionToOptions(Ref* sender);
	void mQuitGame(Ref* sender);

	void mInitMainMenu();
};
#endif

