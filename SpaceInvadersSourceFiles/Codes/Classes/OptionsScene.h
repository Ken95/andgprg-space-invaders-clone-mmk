#ifndef __OPTIONS_SCENE_H__
#define __OPTIONS_SCENE_H__
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

class OptionsScene : public Layer
{
public:
	static Scene* createScene();

	virtual bool init();

	CREATE_FUNC(OptionsScene);

private:
	Size mVisibleSize;
	Vec2 mOrigin;

	Label* mMuteLabel;
	bool mIsMute;

	void mTransitionToTitle(Ref * sender);
	void mMuteAllAudio(Ref * sender);

	void mInitOptionsMenu();
	void mInitMuteText();
};
#endif