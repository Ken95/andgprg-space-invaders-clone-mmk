#include "TitleScene.h"
#include "HelloWorldScene.h"
#include "OptionsScene.h"

Scene* TitleScene::createScene()
{
	auto scene = Scene::create();
	auto layer = TitleScene::create();
	scene->addChild(layer);

	return scene;
}

bool TitleScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Play Menu Background Music
	// Music from Undertale Menu
	// Credits to Toby Fox
	SimpleAudioEngine::getInstance()->playBackgroundMusic("mus_menu0.ogg", true);
	
	// Initialize Screen Size and Origin Point
	mVisibleSize = Director::getInstance()->getVisibleSize();
	mOrigin = Director::getInstance()->getVisibleOrigin();


	auto logo = Sprite::create("Logo.jpg");
	logo->setPosition(Vec2(mOrigin.x + mVisibleSize.width/2, mOrigin.y + mVisibleSize.height * 0.6f));
	this->addChild(logo);
	// Create Main Menu with Play, Options and Quit Button
	mInitMainMenu();

	return true;
}


void TitleScene::mTransitionToPlay(Ref * sender)
{
	// Play Button Press sound effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Create a Main Game Scene and transition from here to there
	Scene* gameScene = HelloWorld::createScene();
	Director::getInstance()->pushScene(gameScene);
}

void TitleScene::mTransitionToOptions(Ref * sender)
{
	// Play Button Press sound effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Create an Options Scene and transition from here to there
	Scene* optionsScene = OptionsScene::createScene();
	Director::getInstance()->pushScene(optionsScene);
}

void TitleScene::mQuitGame(Ref * sender)
{
	// Exits the Application
	Director::getInstance()->end();
}

void TitleScene::mInitMainMenu()
{
	// Create a Play Label and Make it a menu item that will transition to the Main Game Scene
	auto playLabel = Label::createWithTTF("PLAY", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* play = MenuItemLabel::create(playLabel, CC_CALLBACK_1(TitleScene::mTransitionToPlay, this));

	// Create an Options Label and Make it a menu item that will transition to the Options Scene
	auto optionsLabel = Label::createWithTTF("OPTIONS", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* options = MenuItemLabel::create(optionsLabel, CC_CALLBACK_1(TitleScene::mTransitionToOptions, this));
	options->setPositionY(options->getPositionY() - options->getContentSize().height);

	// Create a Quit Label and make it a menu item that will exit the game
	auto quitLabel = Label::createWithTTF("QUIT", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* quit = MenuItemLabel::create(quitLabel, CC_CALLBACK_1(TitleScene::mQuitGame, this));
	quit->setPositionY(options->getPositionY() - quit->getContentSize().height);

	// After making Menu items, Make a menu that will contain the items
	Menu* menu = Menu::create(play, options, quit, NULL);
	menu->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height *0.4f));

	this->addChild(menu);
}

