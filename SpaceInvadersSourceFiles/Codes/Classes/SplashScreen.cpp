#include "SplashScreen.h"
#include "TitleScene.h"

Scene* SplashScreen::createScene()
{
	auto scene = Scene::create();
	auto layer = SplashScreen::create();
	scene->addChild(layer);

	return scene;
}

bool SplashScreen::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	
	mLoadingPercent = 0;

	mLoadingText = Label::createWithTTF("LOADING...", "fonts/EBGaramond-Regular.ttf", 25);
	mLoadingText->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 2));
	this->addChild(mLoadingText);
	
	this->schedule(schedule_selector(SplashScreen::update), 0.05f);

	return true;
}

void SplashScreen::update(float dt)
{
	if (mLoadingPercent >= 100) {
		Scene* titleScene = TitleScene::createScene();
		Director::getInstance()->replaceScene(titleScene);
	}

	mLoadingPercent += 1;

	char loadingText[100] = { 0 };
	sprintf(loadingText, "Loading...     %d", mLoadingPercent);
	mLoadingText->setString(loadingText);
}