#ifndef __RESULTS_SCENE_H__
#define __RESULTS_SCENE_H__
#include "cocos2d.h"

USING_NS_CC;

class ResultsScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(ResultsScene);

private:
	Size mVisibleSize;
	Vec2 mOrigin;

	int mScore;
	int mHighScore;

	void mTransitionToTitle(Ref* sender);
	void mReplay(Ref* sender);

	void mDisplayScore();
	void mDisplayHighScore();
	void mUpdateHighScore();

	void mInitScores();
	void mInitResultMenu();
};
#endif
