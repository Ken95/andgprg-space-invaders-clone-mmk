#include "ResultsScene.h"
#include "TitleScene.h"
#include "HelloWorldScene.h"

Scene* ResultsScene::createScene()
{
	auto scene = Scene::create();
	auto layer = ResultsScene::create();
	scene->addChild(layer);

	return scene;
}

bool ResultsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Initialize Screen Size and Origin
	mVisibleSize = Director::getInstance()->getVisibleSize();
	mOrigin = Director::getInstance()->getVisibleOrigin();

	// Initialize Score for the Current Run and The High Score
	mInitScores();

	// Update HighScore if HighScore is Beaten
	if (mScore > mHighScore) {
		mUpdateHighScore();
	}

	// Display The Scores
	mDisplayScore();
	mDisplayHighScore();

	// Create The Result Menu
	mInitResultMenu();
	
	return true;
}

void ResultsScene::mTransitionToTitle(cocos2d::Ref * sender)
{
	// Play Button Press Sound Effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Transition to the Title Scene
	Scene* titleScene = TitleScene::createScene();
	Director::getInstance()->pushScene(titleScene);
}

void ResultsScene::mReplay(cocos2d::Ref * sender)
{
	// Play Button Press Sound Effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Transition to the Main Game Scene
	Scene* gameScene = HelloWorld::createScene();
	Director::getInstance()->pushScene(gameScene);
}

void ResultsScene::mDisplayScore()
{
	// Create a Score Text based on the current score for the last run
	char scoreText[100] = { 0 };
	sprintf(scoreText, "Score: %d", mScore);

	// Create a label (based on the score text) that will display the score of the current run
	auto scoreLabel = Label::createWithTTF(scoreText, "fonts/EBGaramond-Regular.ttf", 25);
	scoreLabel->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height * 0.85));

	// If High Score is Beaten, Give a feed back to the player by displaying "NEW HIGH SCORE"
	if (mScore > mHighScore) {
		char high[100] = { 0 };
		sprintf(high, "NEW HIGH SCORE: %d", mScore);
		scoreLabel->setString(high);
	}

	this->addChild(scoreLabel);
}

void ResultsScene::mDisplayHighScore()
{
	// Create a High Score Text based on the highest score of all time
	char highScoreText[100] = { 0 };
	sprintf(highScoreText, "High Score: %d", mHighScore);

	// Create a label (based on the high score text) that will display the high score of all time
	auto highScore = Label::createWithTTF(highScoreText, "fonts/EBGaramond-Regular.ttf", 25);
	highScore->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height * 0.75));

	this->addChild(highScore);
}

void ResultsScene::mUpdateHighScore()
{
	// Set the new high score to the current score that you got in the last run
	UserDefault::getInstance()->setIntegerForKey("HIGHSCORE", mScore);

	// Update The High score Variable
	mHighScore = UserDefault::getInstance()->getIntegerForKey("HIGHSCORE");
}

void ResultsScene::mInitScores()
{
	// get the score of the last run
	mScore = UserDefault::getInstance()->getIntegerForKey("SCORE");

	// get the high score of all time
	mHighScore = UserDefault::getInstance()->getIntegerForKey("HIGHSCORE");
}

void ResultsScene::mInitResultMenu()
{
	// Create a Replay Level and make it a menu item that will reload the Main Game Scene
	auto replayLabel = Label::createWithTTF("REPLAY", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* replay = MenuItemLabel::create(replayLabel, CC_CALLBACK_1(ResultsScene::mReplay, this));

	// Create a Back to Main Menu Label and make it a menu item that will transition to the Title Scene
	auto backToMainMenuLabel = Label::createWithTTF("BACK TO MAIN MENU", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* backToMainMenu = MenuItemLabel::create(backToMainMenuLabel, CC_CALLBACK_1(ResultsScene::mTransitionToTitle, this));
	backToMainMenu->setPositionY(backToMainMenu->getPositionY() - backToMainMenu->getContentSize().height);

	// After Creating The menu items, Create the Menu
	Menu* menu = Menu::create(replay, backToMainMenu, NULL);
	menu->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height / 2));

	this->addChild(menu);
}