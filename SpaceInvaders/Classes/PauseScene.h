#ifndef __PAUSE_SCENE_H__
#define __PAUSE_SCENE_H__
#include "cocos2d.h"

USING_NS_CC;

class PauseScene : public Layer
{
public:
	static Scene* createScene();

	virtual bool init();

	CREATE_FUNC(PauseScene);

private:
	Size mVisibleSize;
	Vec2 mOrigin;

	void mResume(Ref* sender);
	void mTransitionToTitle(Ref* sender);

	void mInitPauseMenu();
};
#endif