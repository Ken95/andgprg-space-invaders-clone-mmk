#pragma once
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;

class Unit
{
public:
	Unit(Sprite* sprite);
	~Unit();

	void moveLeft();
	void moveRight();

	Vec2 getPosition();
	Sprite* getSprite();

	void checkEdges();
private:
	Sprite* mSprite;
	float mSpeed;
};

