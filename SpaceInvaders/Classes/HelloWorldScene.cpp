#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "PauseScene.h"
#include "ResultsScene.h"

USING_NS_CC;
using namespace CocosDenshion;

Scene* HelloWorld::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	SimpleAudioEngine::getInstance()->playBackgroundMusic("mus_core.ogg", true);
	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
		origin.y + closeItem->getContentSize().height / 2));

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	auto label = Label::createWithTTF("Pause", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* item = MenuItemLabel::create(label, CC_CALLBACK_1(HelloWorld::mPause, this));

	Menu* m = Menu::create(item, NULL);
	m->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height * 0.9f));

	this->addChild(m, 1);
	/////////////////////////////
	// 3. add your codes below...
	mScore = 0;
	mScoreLabel = Label::createWithTTF("Score: 0", "fonts/Marker Felt.ttf", 12);
	mScoreLabel->setPositionX(origin.x + mScoreLabel->getContentSize().width);
	mScoreLabel->setPositionY(visibleSize.height - mScoreLabel->getContentSize().height / 2);

	mLives = 3;
	mLivesLabel = Label::createWithTTF("Lives: 3", "fonts/Marker Felt.ttf", 12);
	mLivesLabel->setPositionX(visibleSize.width - mLivesLabel->getContentSize().width);
	mLivesLabel->setPositionY(visibleSize.height - mLivesLabel->getContentSize().height / 2);
	mInitPlayer(0);

	mDeltaX = 3.0f;
	mLeftButton = Sprite::create("Left Button.png");
	mRightButton = Sprite::create("Right Button.png");


	this->schedule(schedule_selector(HelloWorld::mEnemyMove), 0.2f);
	this->schedule(schedule_selector(HelloWorld::mEnemyShoot), 3.0f);


	mLeftButton->setPosition(Vec2(origin.x + visibleSize.width * 0.10f,
		origin.y + visibleSize.height * 0.10f));
	mRightButton->setPosition(Vec2(origin.x + visibleSize.width * 0.20f,
		origin.y + visibleSize.height * 0.10f));

	mLeftButtonTouched = false;
	mRightButtonTouched = false;


	EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	this->scheduleUpdate();
	mCreateEnemy();

	this->addChild(mLeftButton);
	this->addChild(mRightButton);
	this->addChild(mScoreLabel);
	this->addChild(mLivesLabel);
	return true;
}

bool HelloWorld::onTouchBegan(cocos2d::Touch * touch, cocos2d::Event * event)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	if (mLeftButton->getBoundingBox().containsPoint(touch->getLocation())) {
		mLeftButtonTouched = true;
		mRightButtonTouched = false;
	}
	else if (mRightButton->getBoundingBox().containsPoint(touch->getLocation())) {
		mRightButtonTouched = true;
		mLeftButtonTouched = false;
	}
	else if (mPlayerBullet == NULL && mPlayer != NULL) {
		SimpleAudioEngine::getInstance()->playEffect("shoot.wav");

		mPlayerBullet = Sprite::create("Player Bullet.png");
		mPlayerBullet->setPosition(mPlayer->getPosition());
		mPlayerBullet->setScale(0.1f);

		MoveTo* actionMove = MoveTo::create(0.75f, Vec2(mPlayerBullet->getPositionX(), visibleSize.width));
		CallFuncN* actionMoveFinished = CallFuncN::create(CC_CALLBACK_1(HelloWorld::mActionFinished, this));

		Sequence* shoot = Sequence::create(actionMove, actionMoveFinished, NULL);

		mPlayerBullet->runAction(shoot);
		this->addChild(mPlayerBullet);
	}
	return true;
}

void HelloWorld::onTouchMoved(cocos2d::Touch * touch, cocos2d::Event * event)
{
	if (mLeftButton->getBoundingBox().containsPoint(touch->getLocation())) {
		mLeftButtonTouched = true;
		mRightButtonTouched = false;
	}
	else if (mRightButton->getBoundingBox().containsPoint(touch->getLocation())) {
		mRightButtonTouched = true;
		mLeftButtonTouched = false;
	}
	else {
		mRightButtonTouched = false;
		mLeftButtonTouched = false;
	}
}

void HelloWorld::onTouchEnded(cocos2d::Touch * touch, cocos2d::Event * event)
{
	mRightButtonTouched = false;
	mLeftButtonTouched = false;
}

void HelloWorld::mEnemyMove(float dt)
{
	bool willMoveDown = false;

	if (mEnemies.size() <= 0) {
		mCreateEnemy();
		mLives++;
		char livesText[100] = { 0 };
		sprintf(livesText, "Lives: %d", mLives);
		mLivesLabel->setString(livesText);
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	for (int i = 0; i < mEnemies.size(); i++) {
		mEnemies.at(i)->setPositionX(mEnemies.at(i)->getPositionX() + mDeltaX);

		if (mEnemies.at(i)->getPositionX() >= visibleSize.width * 0.95f || mEnemies.at(i)->getPositionX() <= visibleSize.width *  0.05f) {
			willMoveDown = true;
		}
	}

	bool gameOver = false;
	if (willMoveDown) {
		for (int i = 0; i < mEnemies.size(); i++) {
			mEnemies.at(i)->setPositionY(mEnemies.at(i)->getPositionY() - 5.0f);
			if (mEnemies.at(i)->getPositionY() <= origin.y + visibleSize.height * 0.275f) gameOver = true;
		}
		
		if (gameOver) {
			UserDefault::getInstance()->setIntegerForKey("SCORE", mScore);
			Scene* s = ResultsScene::createScene();
			Director::getInstance()->replaceScene(s);
		}
		mDeltaX *= -1.0f;
	}

}

void HelloWorld::mEnemyShoot(float dt)
{

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	int r = rand() % mFrontLine.size();
	for (int i = 0; i < mFrontLine.size(); ++i) {
		if (i == r)
		{
			mEnemyBullet = Sprite::create("Enemy Laser.png");
			mEnemyBullet->setScale(0.1f);
			mEnemyBullet->setPosition(mFrontLine.at(i)->getPosition());

			MoveTo* actionMove = MoveTo::create(0.75f, Vec2(mFrontLine.at(i)->getPositionX(), 0.0f));
			CallFuncN* actionMoveFinished = CallFuncN::create(CC_CALLBACK_1(HelloWorld::mActionEnemyFinished, this));

			Sequence* shoot = Sequence::create(actionMove, actionMoveFinished, NULL);

			mEnemyBullet->runAction(shoot);
			this->addChild(mEnemyBullet);
		}
	}
}

void HelloWorld::mEnemyDestroy()
{
	if (mEnemies.size() <= 0) return;

	for (int i = 0; i < mFrontLine.size(); ++i) {
		if (mFrontLine.at(i)->getBoundingBox().containsPoint(mPlayerBullet->getPosition())) {
			int x = mFrontLine.at(i)->getTag();

			this->removeChild(mFrontLine.at(i));
			mFrontLine.eraseObject(mFrontLine.at(i), false);

			for (int i = 0; i < mEnemies.size(); ++i) {
				if (mEnemies.at(i)->getTag() == x - 10) {
					mFrontLine.pushBack(mEnemies.at(i));
					break;
				}
			}

			break;
		}
	}

	for (int i = 0; i < mEnemies.size(); ++i) {
		if (mEnemies.at(i)->getBoundingBox().containsPoint(mPlayerBullet->getPosition())) {

			if (mEnemies.at(i)->getTag() > 40) {
				mScore += 10;
				char scoreText[100] = { 0 };
				sprintf(scoreText, "Score: %d", mScore);
				mScoreLabel->setString(scoreText);
			}

			if (mEnemies.at(i)->getTag() > 20 && mEnemies.at(i)->getTag() <= 40) {
				mScore += 50;
				char scoreText[100] = { 0 };
				sprintf(scoreText, "Score: %d", mScore);
				mScoreLabel->setString(scoreText);
			}

			if (mEnemies.at(i)->getTag() <= 20) {
				mScore += 100;
				char scoreText[100] = { 0 };
				sprintf(scoreText, "Score: %d", mScore);
				mScoreLabel->setString(scoreText);
			}
			SimpleAudioEngine::getInstance()->playEffect("invaderkilled.wav");
			this->removeChild(mEnemies.at(i));
			mEnemies.eraseObject(mEnemies.at(i), false);

			this->removeChild(mPlayerBullet);
			mPlayerBullet = NULL;

			break;
		}
	}
}

void HelloWorld::mPlayerDestroy()
{
	if (mEnemyBullet->getBoundingBox().intersectsRect(mPlayer->getSprite()->getBoundingBox())) {
		this->removeChild(mEnemyBullet);
		mEnemyBullet = NULL;

		this->removeChild(mPlayer->getSprite());
		mPlayer = NULL;

		SimpleAudioEngine::getInstance()->playEffect("explosion.wav");
		mLives--;
		if (mLives <= 0) {
			UserDefault::getInstance()->setIntegerForKey("SCORE", mScore);
			Scene* s = ResultsScene::createScene();
			Director::getInstance()->replaceScene(s);
		}

		char livesText[100] = { 0 };
		sprintf(livesText, "Lives: %d", mLives);
		mLivesLabel->setString(livesText);

		this->scheduleOnce(schedule_selector(HelloWorld::mInitPlayer), 1.0f);
	}
}

void HelloWorld::mInitPlayer(float dt)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	cocos2d::Sprite* player;

	player = Sprite::create("Player Sprite.png");
	player->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height * 0.2f + origin.y));
	player->setScale(0.2f);

	mPlayer = new Unit(player);

	this->addChild(player, 0);
}


void HelloWorld::update(float dt)
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	if (mPlayer != NULL) {
		if (mLeftButtonTouched) {
			mPlayer->moveLeft();
		}
		else if (mRightButtonTouched) {
			mPlayer->moveRight();
		}


		mPlayer->checkEdges();
		if (mPlayerBullet != NULL) {
			mEnemyDestroy();
		}

		if (mEnemyBullet != NULL) {
			mPlayerDestroy();
		}
	}
}

void HelloWorld::mActionFinished(Ref* sender)
{
	Sprite* sprite = static_cast<Sprite*>(sender);
	this->removeChild(sprite);
	mPlayerBullet = NULL;
}

void HelloWorld::mActionEnemyFinished(Ref * sender)
{
	Sprite* sprite = static_cast<Sprite*>(sender);
	this->removeChild(sprite);
	mEnemyBullet = NULL;
}

void HelloWorld::mCreateEnemy()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Sprite* enemy;

	for (int i = 1; i <= 10; i++) {
		enemy = Sprite::create("EnemyShip1a.png");
		enemy->setTag(i + 40);
		enemy->setName("Front Liner");
		enemy->setPosition(Vec2(i * 33.33f, visibleSize.height * 0.5f));
		enemy->setScale(0.15f);
		this->addChild(enemy);

		mEnemies.pushBack(enemy);
		mFrontLine.pushBack(enemy);
	}
	for (int i = 1; i <= 10; i++) {
		enemy = Sprite::create("EnemyShip1a.png");
		enemy->setTag(i + 30);
		enemy->setName("");
		enemy->setPosition(Vec2(i * 33.33f, visibleSize.height * 0.6f));
		enemy->setScale(0.15f);
		this->addChild(enemy);

		mEnemies.pushBack(enemy);
	}
	for (int i = 1; i <= 10; i++) {
		enemy = Sprite::create("EnemyShip2a.png");
		enemy->setTag(i + 20);
		enemy->setName("");
		enemy->setPosition(Vec2(i * 33.33f, visibleSize.height * 0.7f));
		enemy->setScale(0.15f);
		this->addChild(enemy);

		mEnemies.pushBack(enemy);
	}
	for (int i = 1; i <= 10; i++) {
		enemy = Sprite::create("EnemyShip2a.png");
		enemy->setTag(i + 10);
		enemy->setName("");
		enemy->setPosition(Vec2(i * 33.33f, visibleSize.height * 0.8f));
		enemy->setScale(0.15f);
		this->addChild(enemy);

		mEnemies.pushBack(enemy);
	}
	for (int i = 1; i <= 10; i++) {
		enemy = Sprite::create("EnemyShip3a.png");
		enemy->setTag(i);
		enemy->setName("");
		enemy->setPosition(Vec2(i * 33.33f, visibleSize.height * 0.9f));
		enemy->setScale(0.15f);
		this->addChild(enemy);

		mEnemies.pushBack(enemy);
	}
}

void HelloWorld::mPause(cocos2d::Ref * sender)
{
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");
	Scene* s = PauseScene::createScene();
	TransitionMoveInL * t = TransitionMoveInL::create(0.2f, s);
	Director::getInstance()->pushScene(t);
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);
}


