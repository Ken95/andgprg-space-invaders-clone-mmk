#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Unit.h"
#include <vector>
class HelloWorld : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);

	void update(float dt);
	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);

private:
	Unit* mPlayer;
	cocos2d::Sprite* mPlayerBullet;

	cocos2d::Vector<cocos2d::Sprite*> mEnemies;
	cocos2d::Vector<cocos2d::Sprite*> mFrontLine;
	cocos2d::Sprite* mEnemyBullet;

	cocos2d::Sprite* mLeftButton;
	bool mLeftButtonTouched;

	cocos2d::Sprite* mRightButton;
	bool mRightButtonTouched;
	
	float mDeltaX;

	void mActionFinished(Ref* sender);
	void mActionEnemyFinished(Ref* sender);
	void mCreateEnemy();
	void mPause(cocos2d::Ref * sender);
	void mEnemyMove(float dt);
	void mEnemyShoot(float dt);

	void mEnemyDestroy();
	void mPlayerDestroy();
	void mInitPlayer(float dt);

	cocos2d::Label* mScoreLabel;
	int mScore;

	cocos2d::Label* mLivesLabel;
	int mLives;
};

#endif // __HELLOWORLD_SCENE_H__
