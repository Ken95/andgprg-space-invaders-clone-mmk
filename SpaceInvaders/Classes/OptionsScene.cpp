#include "OptionsScene.h"
#include "TitleScene.h"

Scene* OptionsScene::createScene()
{
	auto scene = Scene::create();
	auto layer = OptionsScene::create();
	scene->addChild(layer);

	return scene;
}

bool OptionsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Initialize Screen Size and Origin Point
	mVisibleSize = Director::getInstance()->getVisibleSize();
	mOrigin = Director::getInstance()->getVisibleOrigin();

	// Create The Options Menu
	mInitOptionsMenu();
	mInitMuteText();

	return true;
}

void OptionsScene::mTransitionToTitle(Ref * sender)
{
	// Play the Button press sound effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Go back to the Title Scene
	Director::getInstance()->popScene();
}

void OptionsScene::mMuteAllAudio(Ref * sender)
{
	if (mIsMute) {
		// If all audio is muted and the player clicks the Label, All Audio will be Unmuted
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
		SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);

		// Since all Audio will be unmuted, The Label text will be "Mute"
		mIsMute = false;
		mMuteLabel->setString("MUTE");
	}
	else {
		// If all audio is Playing and the player clicks the Label, All Audio will be Muted
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
		SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);

		// Since all Audio will be muted, The Label text will be "Mute"
		mIsMute = true;
		mMuteLabel->setString("UNMUTE");
	}

	// Play the Button Press Sound Effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");
}

void OptionsScene::mInitOptionsMenu()
{
	// Create a mute/unmute label and make it an menu item that will set the volume of all audio in the game
	mMuteLabel = Label::createWithTTF("MUTE", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* mute = MenuItemLabel::create(mMuteLabel, CC_CALLBACK_1(OptionsScene::mMuteAllAudio, this));

	// Create a back label and make it an menu item that will Transition back to the Title Scene
	auto backLabel = Label::createWithTTF("BACK", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* back = MenuItemLabel::create(backLabel, CC_CALLBACK_1(OptionsScene::mTransitionToTitle, this));
	back->setPositionY(back->getPositionY() - back->getContentSize().height);

	// After Creating all the Menu Items, Create the menu
	Menu* menu = Menu::create(mute, back, NULL);
	menu->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height / 2));

	this->addChild(menu);
}

void OptionsScene::mInitMuteText()
{
	// If the background music isn't muted, set the Mute Label to "MUTE"
	if (SimpleAudioEngine::getInstance()->getBackgroundMusicVolume() == 1.0f) {
		mIsMute = false;
		mMuteLabel->setString("MUTE");
	}

	// If the background music is muted, set the Mute Label to "UNMUTE"
	else if (SimpleAudioEngine::getInstance()->getBackgroundMusicVolume() == 0.0f) {
		mMuteLabel->setString("UNMUTE");
		mIsMute = true;
	}
}