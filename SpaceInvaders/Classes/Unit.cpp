#include "Unit.h"



Unit::Unit(Sprite* sprite)
{
	mSprite = sprite;
	mSpeed = 3.0f;
}


Unit::~Unit()
{
}

void Unit::moveLeft()
{
	mSprite->setPositionX(mSprite->getPositionX() - mSpeed);
}

void Unit::moveRight()
{
	mSprite->setPositionX(mSprite->getPositionX() + mSpeed);
}

Vec2 Unit::getPosition()
{
	return mSprite->getPosition();
}

Sprite * Unit::getSprite()
{
	return mSprite;
}

void Unit::checkEdges()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	if (mSprite->getPositionX() >= origin.x + visibleSize.width * 0.95f) {
		mSprite->setPositionX(origin.x + visibleSize.width * 0.95f);
	}
	if (mSprite->getPositionX() <= origin.x + visibleSize.width * 0.05f) {
		mSprite->setPositionX(origin.x + visibleSize.width * 0.05f);
	}
}
