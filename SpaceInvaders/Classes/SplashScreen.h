#ifndef __SPLASH_SCREEN_H__
#define __SPLASH_SCREEN_H__
#include "cocos2d.h"

USING_NS_CC;

class SplashScreen : public Layer
{
public:
	static Scene* createScene();

	virtual bool init();
	
	void update(float dt);
	
	CREATE_FUNC(SplashScreen);

private:
	int mLoadingPercent;
	Label* mLoadingText;
};
#endif