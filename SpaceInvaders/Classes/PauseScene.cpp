#include "PauseScene.h"
#include "TitleScene.h"
#include "HelloWorldScene.h"

Scene* PauseScene::createScene()
{
	auto scene = Scene::create();
	auto layer = PauseScene::create();
	scene->addChild(layer);

	return scene;
}

bool PauseScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Initialize Screen Size and Origin
	mVisibleSize = Director::getInstance()->getVisibleSize();
	mOrigin = Director::getInstance()->getVisibleOrigin();

	// Create the Pause Menu
	mInitPauseMenu();
	
	return true;
}


void PauseScene::mResume(Ref * sender)
{
	// Play the Button Press Sound Effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Transition Back to the Main Game Scene
	Director::getInstance()->popScene();
}

void PauseScene::mTransitionToTitle(Ref * sender)
{
	// Play the Button Press Sound Effect
	SimpleAudioEngine::getInstance()->playEffect("button.mp3");

	// Transition to the Title Scene
	Scene* titleScene = TitleScene::createScene();
	Director::getInstance()->replaceScene(titleScene);
}

void PauseScene::mInitPauseMenu()
{
	// Create A Resume Label and make it a menu item that will go back to the Main Game Scene
	auto resumeLabel = Label::createWithTTF("RESUME", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* resume = MenuItemLabel::create(resumeLabel, CC_CALLBACK_1(PauseScene::mResume, this));

	// Create A Back To Main Menu Label and make it a menu item that will go back to the Title Scene
	auto backToMenuLabel = Label::createWithTTF("BACK TO MAIN MENU", "fonts/EBGaramond-Regular.ttf", 25);
	MenuItemLabel* backToMenu = MenuItemLabel::create(backToMenuLabel, CC_CALLBACK_1(PauseScene::mTransitionToTitle, this));
	backToMenu->setPositionY(backToMenu->getPositionY() - backToMenu->getContentSize().height);

	// After Creating menu items, Create the Menu
	Menu* menu = Menu::create(resume, backToMenu, NULL);
	menu->setPosition(Vec2(mOrigin.x + mVisibleSize.width / 2, mOrigin.y + mVisibleSize.height / 2));

	this->addChild(menu);
}
